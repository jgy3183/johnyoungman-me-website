module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
    // defaultLineHeights: true,
    // standardFontWeights: true
  },
    purge: {
        mode: 'all',
        preserveHtmlElements: false,
        layers: ['components', 'utilities','base','screens'],
        options: {
            // keyframes: true,
        },
        content: [
          'app/Http/Controllers/MainController.php',
          './resources/views/layouts/main.blade.php',
          './resources/views/home.blade.php',
          './resources/js/**/*.js',
          './resources/js/*.js',
  ]},
  theme: {
    extend: {}
  },
  variants: {},
  plugins: [
    require('postcss-100vh-fix')
  ]
}
