const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss')

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

let jsFiles =
    [
        'resources/js/app.js',
        'resources/js/slidemenu.js'
    ];

mix.js(jsFiles, 'public/js')
    .sass('resources/scss/main.scss', 'public/css')
    .options({
        processCssUrls: false,
        postCss:
            [
                tailwindcss('./tailwind.config.js')
        ],
    })
    // .postCss('resources/css/app.css', 'public/css', [
    //     //
    //     require('tailwindcss'),
    //
    // ]);


