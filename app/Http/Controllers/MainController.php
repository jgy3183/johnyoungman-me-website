<?php

namespace App\Http\Controllers;

use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use function compact;
use function now;

class MainController extends Controller
{
    function home()
    {
        $sections = Cache::remember('sections', now()->addDays(30), function () {
            return [
                [
                    'heading' => 'Who am I?',
                    'content' => '
            <ul>
                <li>I am a problem solver that takes advantage of my Computer Science degree, and extensive background
                        as a software engineer.</li>

                <li>I enjoy tackling challenges and providing efficient solutions.</li>
            </ul>
            ',
                ],
                [
                    'heading' => 'What do I do?',
                    'content' => '
            <ul>
                <li>I enjoy architecting solutions, development, and all things technology!</li>
                <li>I have a strong full-stack background in many tech stacks, languages, & databases.</li>
                <li>I thrive on learning new technologies to maintain a well-rounded background.</li>
            </ul>',
                ],
                [
                    'heading' => 'Skills &amp; Experience',
                    'content' => '
                        <ul>
                                 <li>Learning &amp; adapting to new tech stacks</li>
                                 <li>Breaking big problems into smaller, more digestible ones</li>
                                 <li>Integrations / API development</li>
                                 <hr />
                         </ul>
                    <div class="flex justify-between">
                        <div>
                            <ul>
                                 <li>macOSX</li>
                                 <li>Linux / Apache / Nginx</li>
                                 <li>Windows / IIS</li>
                             </ul>
                        </div>

                        <div>
                            <ul>
                                <li>php / LAMP</li>
                                <li>mySQL/MS SQL Server</li>
                                <li>.NET / C# / Azure Functions / Azure</li>
                                <li>HTML, CSS/SCSS, Javascript</li>
                                <li>Some React &amp; NextJS</li>
                                <li>git</li>
                            </ul>
                        </div>
                    </div>


                </ul>
            ',
                ],
                [
                    'heading' => 'Tools I &#10084;&#65039;',
                    'content' => '

                    <div class="flex justify-between">
                        <div>
                             <ul>
                                 <li>JetBrains (PHPStorm, Rider, WebStorm, DataGrip)</li>
                                 <li>Visual Studio</li>
                                 <li>Sublime Text</li>
                             </ul>
                        </div>

                        <div>
                            <ul>
                                <li>Insomnia & Postman</li>
                                <li>Notion.so</li>
                                <li>Mamp Pro</li>
                                <li>Shift</li>
                            </ul>
                        </div>
                    </div>


                </ul>
            ',
                ],
                [
                    'heading' => 'Contact me',
                    'content' => '
            <ul>
                <li>Email me: <a class="text-blue-300 underline" href="mailto:john.g.youngman@gmail.com">john.g.youngman@gmail.com</a></li>
                <li>Find me <a target=_blank rel="noopener" class="text-blue-300 underline" href="https://www.linkedin.com/in/johngyoungman/">here on LinkedIn</a></li>
            </ul>
          ',
                ],
            ];
        });

        return view('home', compact('sections'));
    }

}
