import smoothscroll from 'smoothscroll-polyfill';

// self executing function here
(function() {
    // your page initialization code here
    // the DOM will be available here


    smoothscroll.polyfill();
    const sections = [...document.querySelectorAll("section")];

    let options = {
        rootMargin: "0px",
        threshold: 0.25
    };

    const callback = (entries, observer) => {
        entries.forEach(entry => {
            const { target } = entry;
            //console.log(entry, target)

            if (entry.intersectionRatio >= 0.25) {
                let targetId = target.id;
                let _sel = "a[href='#" + targetId + "']";
                let relativeMenuA = document.querySelector(_sel);
                updateMenutItemsActive(relativeMenuA);

                target.classList.add("is-visible");
            } else {
                target.classList.remove("is-visible");
            }
        });
    };

    const observer = new IntersectionObserver(callback, options);

    sections.forEach((section, index) => {
        observer.observe(section);
    });


    let downArrows = document.querySelectorAll("div.down-arrow");
    downArrows.forEach(function(el){
        el.addEventListener('click',function(){

            var _h = document.querySelector(".scroll-container").scrollHeight * 1;
            _h = _h / sections.length;
            document.querySelector(".scroll-container").scrollBy({
                "behavior": "smooth",
                "left": 0,
                "top": _h
            });

        });
    });

    let upArrows = document.querySelectorAll("div.up-arrow");
    upArrows.forEach(function(el){
        el.addEventListener('click',function(e){
            var _h = document.querySelector(".scroll-container").scrollHeight * -1;
            _h = _h / sections.length;
            document.querySelector(".scroll-container").scrollBy({
                "behavior": "smooth",
                "left": 0,
                "top": _h
            });

        });
    });



    //add menu

    const slideMenuContainer = document.createElement("div");
    const _ul = document.createElement("ul");


    const _main = document.querySelector("main");


    slideMenuContainer.setAttribute("id","slideMenuContainer");
    slideMenuContainer.setAttribute("class","flex absolute slide-menu align-center");

    sections.forEach(function(section,i){

        let _slideId = "slide" + i;
        section.setAttribute("id",_slideId);

        let _headingDiv = section.querySelector("div.heading");
        let _li = document.createElement("li");
        _li.setAttribute("class","flex bg-white text-black");

        let _a = document.createElement("a");
        _a.setAttribute("href","#" + _slideId);
        _a.addEventListener("click",slideScroll);
        _a.innerHTML=_headingDiv.innerHTML;

        _li.append(_a);

        //console.log(_li.innerHTML);


        _ul.append(_li);


    });

    slideMenuContainer.append(_ul);

    //add menu bubble
    _main.append(slideMenuContainer);

    function slideScroll(e) {
        e.preventDefault();

        const href = this.getAttribute("href");
        const offsetTop = document.querySelector(href).offsetTop;
        //console.log(offsetTop);
        updateMenutItemsActive(this);
        document.querySelector(".scroll-container").scrollTo({
            top: offsetTop,
            left: 0,
            behavior: "smooth"
        });
    }


    function updateMenutItemsActive(e){
        document.querySelector("#slideMenuContainer").querySelectorAll("a")
            .forEach(function(a){
                a.classList.remove("active");
            });
        e.classList.add("active");
    }

})();
