@extends('layouts.main')

@section('title','Home')

@section('content')

    <div class="flex flex-wrap items-center justify-center h-full scroll-container">
        @foreach($sections as $section)
            <section class="info relative flex items-center justify-center w-full h-full">
                <div class="arrow up-arrow absolute top-0">
                    <div class="button">&#8678;</div>
                </div>
                <div class="slide w-2/3 absolute flex flex-wrap items-center justify-center">
{{--                    <div class="arrow up-arrow-action"><button>&#8249;</button></div>--}}


                    <div class="heading w-full text-5xl mb-1 sm-text">
                        {!! $section['heading'] !!}
                    </div>

                    <div class="content w-full text-base md:text-2xl sm:text-base">
                        {!! $section['content'] !!}
                    </div>

{{--                    <div class="arrow down-arrow-action "><button>&#8250;</button></div>--}}

                </div>
                <div class="arrow down-arrow absolute bottom-0">

                    <div class="button">&#8680;</div>

                </div>
{{--                <div class="arrow text-4xl"><button>&#8250;</button></div>--}}
            </section>
        @endforeach
    </div>





@endsection
