<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Google Tag Manager
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-PGHS54H');</script>
  End Google Tag Manager -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-5450619-4"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-5450619-4');
    </script>



    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="Description" content="Website describing a developer, John Youngman of Atlanta, GA.">
{{--    <!-- Fonts -->--}}
{{--    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">--}}

{{--    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Titillium+Web:ital,wght@0,200;0,300;0,400;0,600;0,700;0,900;1,200;1,300;1,400;1,600;1,700&display=swap">--}}

    <style>
        /* latin-ext */
        @font-face {
            font-family: 'Titillium Web';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/titilliumweb/v10/NaPecZTIAOhVxoMyOr9n_E7fdM3mDaZRbryhsA.woff2) format('woff2');
            unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
        }
        /* latin */
        @font-face {
            font-family: 'Titillium Web';
            font-style: normal;
            font-weight: 400;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/titilliumweb/v10/NaPecZTIAOhVxoMyOr9n_E7fdMPmDaZRbrw.woff2) format('woff2');
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
        }
    </style>
{{--        <link rel="preload" href="/css/main.css" as="style" onload="this.onload=null;this.rel='stylesheet'">--}}
    <link rel="stylesheet" href="/css/main.css"/>


{{--    <noscript><link rel="stylesheet" href="/css/main.css"></noscript>--}}

    <title>JohnYoungman.me - @yield('title')</title>

</head>
<body class="relative bg-gray-200">
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PGHS54H"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->

<div class="flex h-full">

    <header class="invisible fixed w-full z-10 py-5 bg-white text-gray-500 text-center">


        <a href="/"><h1>JohnYoungman.me</h1></a>

        <div class="invisible absolute inset-y-0 right-0 flex align-middle">
            <a class="align-middle bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded m-auto mr-2"
               href="mailto:john.g.youngman@gmail.com">
                Send me a note!
            </a>
        </div>

    </header>
    <main class="flex-1 overflow-y-auto">
        @yield('content')
    </main>

    <footer class="flex absolute bottom-0 py-5 text-gray-500 text-center">


            <a class="ml-5" href="/">
                <img class="gravatar m-auto ml-2 opacity-75" width="80" height="80"
                     src="{{ Gravatar::get('john.g.youngman@gmail.com',['size'=>160,'secure'=>TRUE]) }}"
                     alt="my image">
            </a>


    </footer>

</div>

<script type="text/javascript" src="/js/app.js" defer></script>
</body>
</html>
